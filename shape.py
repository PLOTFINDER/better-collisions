import time

from BasicVector import Vec2


class Shape:
    def __init__(self, pos, points):
        self.pos = pos
        self.points = points
        self.collision = False

    def update(self, position):
        self.collision = self.collides(position)

    def collides(self, position):
        result = True

        for i in range(len(self.points)):
            actual_point = self.points[i] + self.pos
            next_point = self.points[(i + 1) % len(self.points)] + self.pos

            points_vector = next_point - actual_point
            mouse_point_vector = actual_point - position

            d = points_vector.x * mouse_point_vector.y - points_vector.y * mouse_point_vector.x

            if d < 0:
                result = False

        return result
