import pygame
from BasicVector import Vec2
from pygame.locals import *
import time

from renderer import renderAll
from shape import Shape


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    pos = Vec2(300, 300)
    a = [Vec2(-50, -100), Vec2(-100, 100), Vec2(-50, 200), Vec2(100, 100), Vec2(150, -100), Vec2(0, -150)]

    shapes = [Shape(pos, a)]

    mouse_pos = None
    pause = False

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Collision")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            if mouse_pos is not None:
                for shape in shapes:
                    shape.update(Vec2(mouse_pos[0], mouse_pos[1]))
            renderAll(window, shapes)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
