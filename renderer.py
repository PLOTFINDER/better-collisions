import pygame.draw


def clear(window):
    window.fill((0, 0, 0))


def renderAll(window, shapes):
    clear(window)
    for shape in shapes:

        pos = shape.pos
        points = shape.points
        collides = shape.collision

        line_color = (255, 0, 0) if collides else (255, 255, 255)

        for i in range(len(points)):
            a = (pos + points[i]).getPos()
            b = (pos + points[(i + 1) % len(points)]).getPos()

            pygame.draw.line(window, line_color, a, b)
